# !/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cache import Cache
from flask_session import Session
from config import DB_CONFIG, CACHE_CONFIG, LOG_LEVEL, SECRT_KEY

db = SQLAlchemy()
cache = Cache()
session = Session()


def create_app():
    f = Flask(__name__)
    f.logger.setLevel = LOG_LEVEL
    f.secret_key = SECRT_KEY

    f.config.update(DB_CONFIG)
    db.init_app(f)

    f.config.update(CACHE_CONFIG)
    cache.init_app(f)

    from app.blueprint.api import api
    f.register_blueprint(api, url_prefix='/api')

    from app.blueprint.user import user
    f.register_blueprint(user, url_prefix='/user')

    return f
