# !/usr/bin/env python
# -*- coding: utf-8 -*-
from app import db


class A10jqkaArticle(db.Model):
    __tablename__ = 'a10jqka_article'
    __bind_key__ = 'data'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.Text(collation=u'utf8mb4_bin'), nullable=False)
    title = db.Column(db.Text(collation=u'utf8mb4_bin'), nullable=False)
    title_md5 = db.Column(db.String(33, u'utf8mb4_bin'), nullable=False, unique=True)
    publish_time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    content = db.Column(db.String(collation=u'utf8mb4_bin'), nullable=False)
    tag = db.Column(db.Integer, nullable=False, server_default=db.FetchedValue())
