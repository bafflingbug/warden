# !/usr/bin/env python
# -*- coding: utf-8 -*-
from app import db


class User(db.Model):
    __tablename__ = 'user'
    __bind_key__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False, unique=True, index=True)
    password = db.Column(db.String(128), nullable=False)
    salt = db.Column(db.String(64), nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    is_delete = db.Column(db.Boolean, nullable=False, default=False)
    first_login = db.Column(db.Boolean, nullable=False, default=True)
