# !/usr/bin/env python
# -*- coding: utf-8 -*-
import hashlib


def md5_update(a, salt=None):
    m = hashlib.md5()
    m.update(a.encode('utf-8'))
    if salt:
        m.update(salt.encode('utf-8'))
    return m.hexdigest()
