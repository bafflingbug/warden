# !/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import json, session
from app.util import get_user

msg_dict = {
    0: '',
    1: '登陆成功',
    10: '请修改密码',
    100: 'ok',
    -1: '用户名或密码错误',
    -3: '无法重复登陆',
    -12: '没有登录',
    -100: '缺少参数',
    -1000: '未知错误'
}


def resp(msg_id=0, msg=None, data=None, status_code=200):
    if msg is None:
        msg = msg_dict.get(msg_id, '')
    return json.dumps({
        'id': msg_id,
        'msg': msg,
        'data': data,
    }), status_code


def resp_with_user(msg_id=0, msg=None, data=None, status_code=200):
    user_id = session.get('userId', None)
    resp_user = None
    if user_id:
        user = get_user(_id=user_id)
        if user:
            resp_user = {
                'id': user.id,
                'name': user.name,
                'is_admin': user.is_admin
            }
    if msg is None:
        msg = msg_dict.get(msg_id, '')
    return json.dumps({
        'id': msg_id,
        'msg': msg,
        'data': data,
        'user': resp_user
    }), status_code
