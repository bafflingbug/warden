# !/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
from redis import Redis

DEFAULT_PATH = os.path.dirname(os.path.abspath(__file__))
FILE_PATH = os.path.join(DEFAULT_PATH, 'file')

DB_CONFIG = {
    'SQLALCHEMY_BINDS': {
        'data': 'mysql+pymysql://root:@127.0.0.1:3306/warden?charset=utf8mb4',
        'user': 'sqlite:///%s' % (os.path.join(FILE_PATH, 'user.db'),)
    },
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'SQLALCHEMY_COMMIT_ON_TEARDOWN': True
}

CACHE_CONFIG = {
    'CACHE_TYPE': 'redis',
    'CACHE_KEY_PREFIX': 'wardenCache_',
    'CACHE_REDIS_URL': 'redis://:@localhost:6379/2'
}

SESSION_CONFIG = {
    'SESSION_TYPE': 'redis',
    'SESSION_REDIS': Redis(host='localhost', port=6379, db=2),
    'SESSION_KEY_PREFIX': 'wardenSession_',
    'SESSION_PERMANENT': True

}

LOG_LEVEL = logging.DEBUG

PALADIN = 'http://127.0.0.1:5344/api/label'

SECRT_KEY = 'c9d4ba13-6cc9-4894-8854-ddc5a714990d'
